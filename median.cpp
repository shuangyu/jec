#include "TH1D.h"
#include <iostream>
#include <TMath.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH2F.h>
#include <string.h>
#include "TTree.h"
#include "TAxis.h"
#include <random>

std::vector<TH1D *> getmedian(TFile *f1)
{
  double etabin[20] = {0.000, 0.261, 0.522, 0.783, 1.044, 1.305, 1.566, 1.740, 1.930, 2.043, 2.172, 2.322,
                       2.500, 2.650, 2.853, 2.964, 3.139, 3.489, 3.839, 5.191};
  double ptbin[37] = {15, 17, 20, 23, 27, 30, 35, 40, 45, 57, 72, 90, 120, 150,
                      200, 300, 400, 550, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500
                      , 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500
                      };
  double *etabins = etabin;
  double *ptbins = ptbin;
  TAxis *etaaxis = new TAxis(sizeof(etabin) / sizeof(etabin[0]) - 1, etabins);
  TAxis *ptaxis = new TAxis(sizeof(ptbin) / sizeof(ptbin[0]) - 1, ptbins);
  
  TTree *median = (TTree *)f1->Get("median");
  double ptgen, etarec, response;
  median->SetBranchAddress("ptgen", &ptgen);
  median->SetBranchAddress("etarec", &etarec);
  median->SetBranchAddress("response", &response);
  int entries = median->GetEntries();
  std::vector<TH1D *> hist_response;

  for (int i = 1; i < sizeof(etabin) / sizeof(etabin[0]); i++)
  {
    std::random_device rd;
    std::uniform_int_distribution<int> dist(1, 100000);
    int random_num = dist(rd);
    TString name = TString::Format("response%d_%d", i, random_num);
    TH1D *response = new TH1D(name, name, sizeof(ptbin) / sizeof(ptbin[0]) - 1, ptbins);
    hist_response.push_back(response);
  }
  std::vector<std::vector<std::vector<double>>> value_response;
  for (int i = 1; i < sizeof(etabin) / sizeof(etabin[0]); i++)
  {
    std::vector<std::vector<double>> vec1;
    for (int j = 1; j < sizeof(ptbin) / sizeof(ptbin[0]); j++)
    {
      std::vector<double> temp = {};
      vec1.push_back(temp);
    }
    value_response.push_back(vec1);
  }
  for (int k = 0; k < entries; k++)
  {
    median->GetEntry(k);
    if (ptgen <= 15 || ptgen >= 5500 || etarec >= etabin[sizeof(etabin) / sizeof(etabin[0]) - 1] || etarec <= 0)
      continue;
    int eta = etaaxis->FindBin(etarec);
    int pt = ptaxis->FindBin(ptgen);
    // std::cout << " " << eta << " " << pt << std::endl;
    value_response.at(eta - 1).at(pt - 1).push_back(response);
  }

  for (int i = 0; i < value_response.size(); i++)
  {
    for (int j = 0; j < value_response.at(i).size(); j++)
    {
      double value_median;
      std::vector<double> vec_response = value_response.at(i).at(j);
      if (vec_response.size() == 0)
      {
        value_median = 0;
      }
      else
      {
        std::sort(vec_response.begin(), vec_response.end());
        int vec_size = vec_response.size();
        if (vec_size % 2 == 0)
          value_median = (vec_response.at(int(vec_size / 2)) + vec_response.at(int(vec_size / 2 - 1))) / 2;
        else
          value_median = vec_response.at(int(vec_size / 2));
      }
      std::cout << value_median << " ";
      hist_response.at(i)->SetBinContent(j, value_median);
      hist_response.at(i)->SetBinError(j, 0.00001);
    }
    std::cout << std::endl;
  }
  return hist_response;
}